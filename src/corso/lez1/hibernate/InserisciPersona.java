package corso.lez1.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import corso.lez1.hibernate.models.Persona;
import corso.lez1.hibernate.models.db.GestoreSessioni;

public class InserisciPersona {

	public static void main(String[] args) {

		SessionFactory sf = GestoreSessioni.getInstance().getFactory();

		Persona perUno = new Persona("Giovanni", "Pace");
		Persona perDue = new Persona("Mario", "Rossi");
		Persona perTre = new Persona("Valeria", "Verdi");
		
		Session sessione = sf.getCurrentSession();
		
		try {
			sessione.beginTransaction();
			sessione.save(perUno);
			sessione.save(perDue);
			sessione.save(perTre);
			sessione.getTransaction().commit();
			
			System.out.println(perUno);
			System.out.println(perDue);
			System.out.println(perTre);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
			System.out.println("Programma terminato");
		}
		
		
	}

}
