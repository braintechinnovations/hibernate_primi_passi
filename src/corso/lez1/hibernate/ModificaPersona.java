package corso.lez1.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import corso.lez1.hibernate.models.Persona;
import corso.lez1.hibernate.models.db.GestoreSessioni;

public class ModificaPersona {

	public static void main(String[] args) {

		SessionFactory sf = GestoreSessioni.getInstance().getFactory();
		
		Session sessione = sf.getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			
			Persona gio = sessione.get(Persona.class, 1);
			gio.setPer_nome("Saul");
			gio.setPer_cogn("Goodman");
			
			sessione.save(gio);
			
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
	}
	
}
