package corso.lez1.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import corso.lez1.hibernate.models.Persona;
import corso.lez1.hibernate.models.db.GestoreSessioni;

public class CercaPersone {

	public static void main(String[] args) {

		SessionFactory sf = GestoreSessioni.getInstance().getFactory();
		
		Session sessione = sf.getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			
			List<Persona> elenco = sessione.createQuery("FROM Persona WHERE per_nome = 'Valeria'").list();
			
			for(int i=0; i<elenco.size(); i++) {
				Persona temp = elenco.get(i);
				System.out.println(temp);
			}
			
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
	}

}
