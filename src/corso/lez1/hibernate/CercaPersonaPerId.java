package corso.lez1.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import corso.lez1.hibernate.models.Persona;
import corso.lez1.hibernate.models.db.GestoreSessioni;

public class CercaPersonaPerId {
	public static void main(String[] args) {

		SessionFactory sf = GestoreSessioni.getInstance().getFactory();
		
		Session sessione = sf.getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			
			Persona val = sessione.get(Persona.class, 3);
			System.out.println(val);
			
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
	}
}
