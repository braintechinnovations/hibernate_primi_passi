package corso.lez1.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import corso.lez1.hibernate.models.db.GestoreSessioni;

public class EliminaPersona {

	public static void main(String[] args) {

		SessionFactory sf = GestoreSessioni.getInstance().getFactory();
		
		Session sessione = sf.getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			
			int affRows = sessione
					.createQuery("DELETE Persona WHERE id = 2")
					.executeUpdate();
			
			if(affRows > 0) {
				System.out.println("Eliminazione effettuata con successo!");
			}
			else {
				System.out.println("Errore di eliminazione");
			}
			
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
	}

}
