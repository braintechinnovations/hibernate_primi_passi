package corso.lez1.hibernate.models.db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import corso.lez1.hibernate.models.Persona;

public class GestoreSessioni {

	private static GestoreSessioni ogg_gestore;
	private SessionFactory factory;
	
	public static GestoreSessioni getInstance() {
		if(ogg_gestore == null) {
			ogg_gestore = new GestoreSessioni();
		}
		
		return ogg_gestore;
	}
	
	public SessionFactory getFactory() {
		if(factory == null) {
			factory = new Configuration()
					.configure("/resources/hibernate_persona.cfg.xml")
					.addAnnotatedClass(Persona.class)
					.buildSessionFactory();
		}
		
		return factory;
	}
	
	
	
}
